/*
tinc project builder
jsFile create on 18.03.2021 15:18:36
*/
$(document).ready(function () {
  if ($('.owl-carousel').length > 0) {
    let newsSlider = $('.newsSlider')
    setNewsSlider(newsSlider)

    if ($(window).width() < 1330) {
      newsSlider.trigger('destroy.owl.carousel')
    }

    $(window).on('resize', function () {
      if ($(window).width() < 1330) {
        newsSlider.trigger('destroy.owl.carousel')
      } else {
        setNewsSlider(newsSlider)
      }
    })

    $('.sliderTemplate .slider').owlCarousel({
      items: 1,
      nav: true,
      dots: true,
      navText: ['<span class="icon-prev"></span>', '<span class="icon-next"></span>']
    })
  }

  if ($('.slickSlider').length > 0) {
    $('.dataSlider').slick({
      dots: true,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 2000,
      speed: 500,
      fade: true,
      cssEase: 'linear',
      arrows: false,
      dotsClass: 'slickSliderDots',
      initialSlide: 1
    });
  }

  if ($('.dropdownControl').length > 0) {
    if ($(window).width() > 1329) {
      setDropdown()
    }

    $(window).on('resize', function () {
      if ($(window).width() > 1329) {
        setDropdown()
      } else {
        $('.dropdownControl').map(function () {
          $(this).off()
        })
      }
    })
  }

  if ($('.navDropdown').length > 0) {
    if ($(window).width() > 1329) {
      setNavDropdown()
    }

    $(window).on('resize', function () {
      if ($(window).width() > 1329) {
        setNavDropdown()
      } else {
        $('.navDropdown').map(function () {
          $(this).off()
        })
      }
    })
  }

  if ($('.openMenuBtn').length > 0) {
    $('.openMenuBtn').on('click', function () {
      if ($(this).hasClass('active')) {
        $(this).siblings('.mobileMenu').stop().fadeOut(500);
        $(this).removeClass('active')
      } else {
        $(this).siblings('.mobileMenu').stop().fadeIn(500);
        $(this).addClass('active')
      }
    })
  }

  if ($('.enterNum').length > 0) {
    $('.enterNum').bind("change keyup input click", function () {
      if (this.value.match(/[^0-9\\.]/gi)) {
        this.value = this.value.replace(/[^0-9\\.]/gi, '');
      }
    });
  }

  if ($('.hideContent').length > 0) {
    let hiddenBlock = $('.hideContent').siblings('.hiddenContent').find('.hiddenBlock')

    $('.hideContent').on('click', function () {

      if ($(this).hasClass('active')) {
        hiddenBlock.stop().slideUp(500);
        $(this).removeClass('active');
        $(this).text('Показать больше фишек');
      } else {
        hiddenBlock.stop().slideDown(500);
        $(this).addClass('active');
        $(this).text('Показать меньше фишек');
      }
    })
  }

  if ($(".modalsScroll").length > 0) {
    openMod();
  }

  if ($('.customScrollbar').length > 0) {
    if ($(window).width() >= 1330) {
      $('.customScrollbar').mCustomScrollbar({
        axis: 'y',
      })
    }

    $(window).on('resize', function () {
      if ($(window).width() >= 1330) {
        $('.customScrollbar').mCustomScrollbar({
          axis: 'y',
        })
      } else {
        $('.customScrollbar').mCustomScrollbar('destroy')
      }
    })
  }

  if ($('.dropdownItem').length > 0) {
    var dropHead = $('.dropdownItem > .head'),
      dropContent = $('.dropdownItem > .content');

    $('.dropdownItem').map(function () {
      var el = $(this)
      if (el.find(dropHead).length > 0 && el.find(dropContent).length > 0) {

        el.find(dropHead).on('click', function () {
          var el = $(this),
            cont = el.siblings(dropContent);

          if (el.hasClass('active')) {
            cont.stop().slideUp(500, function () {
              el.removeClass('active');
            });

          } else {
            cont.stop().slideDown(500, function () {
              el.addClass('active');
            });
          }
        })
      }
    })
  }

  if ($('.tabs').length > 0) {
    $('.tabs').map(function () {
      if ($(this).hasClass('selectControl')) {
        $(this).tabs({
          active: 1,
          create: function () {
            if ($(window).width() >= 768) {
              setSelectric($(this))
            }
          },
          activate: function () {
            if ($(window).width() >= 768) {
              setSelectric($(this))
            }
          }
        })
      } else {
        $(this).tabs()
      }
    })
  }

  if ($('select').length > 0) {
    $('select').map(function () {
      if ($(this).hasClass('tabsControl')) {
        $(this).selectric({
          onInit: function (event) {
            if ($(window).width() < 768) {
              setTabs($(this), event.selectedIndex)
            }
          },
          onChange: function (event) {
            if ($(window).width() < 768) {
              setTabs($(this), event.selectedIndex)
            }
          }
        });
      } else {
        $(this).selectric();
      }
    })
  }

  if ($('.tabsControl').length > 0) {
    $('.tabsControl').map(function(index, element) {
      let selected = element.selectedIndex;
      setTabs($(element), selected);

      $(this).on('change', function(event) {
        setTabs($(this), event.target.selectedIndex);
      })
    })
  }
})

let setNewsSlider = (el) => {
  el.owlCarousel({
    items: 3,
    margin: 80,
    nav: false,
    dots: true,
    startPosition: 3
  })
}

let setDropdown = () => {
  $('.dropdownControl').on('mouseenter', function () {
    $(this).find('.dropdownMenu').stop().fadeIn(500);
    $(this).addClass('active')
  })

  $('.dropdownControl').on('mouseleave', function () {
    $(this).find('.dropdownMenu').stop().fadeOut(500);
    $(this).removeClass('active');
  })
}

let setNavDropdown = () => {
  let closeDropdown;

  $('.navDropdown').on('mouseenter', function () {
    clearTimeout(closeDropdown);
    $(this).find('.navDropdownBlock').stop().fadeIn(500);
    $(this).addClass('active');
  })

  $('.navDropdown').on('mouseleave', function () {
    let el = $(this);

    closeDropdown = setTimeout(() => {
      el.find('.navDropdownBlock').stop().fadeOut(500);
      el.removeClass('active');
    }, 1500);
  })
}

let setSelectric = function (el) {
  let activeTab = el.tabs("option", "active");
  let selectric = el.siblings('.tabsOptions').find('.tabsControl');
  selectric.prop('selectedIndex', activeTab).selectric('refresh');
}

let setTabs = function(el, index) {
  let tabs = el.parents('.tabsOptions').siblings('.tabs.selectControl')
  tabs.tabs('option', 'active', index);
}